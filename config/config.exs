# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :dreck, DreckWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YUuCvMepjFG4oLzi/BrW6LjI5Eji6uymQuOeeraevewRI3h9Ktp2AzzapUh4FrDo",
  render_errors: [view: DreckWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Dreck.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configure Hammer
config :hammer,
  backend: {Hammer.Backend.ETS, [expiry_ms: 60_000 * 60 * 4,
                                 cleanup_interval_ms: 60_000 * 10,
                                 pool_size: 2,
                                 pool_max_overflow: 4]}

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
