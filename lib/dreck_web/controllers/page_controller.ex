defmodule DreckWeb.PageController do
  use DreckWeb, :controller
  import Logger

  plug Hammer.Plug, [
    rate_limit: {"ping", 30_000, 5},
    by: :ip
  ] when action == :ping

  plug Hammer.Plug, [
    rate_limit: {"ping_session", 60_000, 5},
    by: {:session, :user_id},
    when_nil: :use_nil
  ] when action == :ping_session

  plug Hammer.Plug, [
    rate_limit: {"ping_session_deep", 60_000, 5},
    by: {:session, :user, &DreckWeb.PageController.get_user_id/1},
    when_nil: :use_nil
  ] when action == :ping_session_deep

  def get_user_id(user) do
    user.id
  end

  def index(conn, _params) do
    Logger.info("User: #{get_session(conn, :user_id)}")
    render conn, "index.html"
  end

  def ping(conn, _params) do
    render conn, "pong.html"
  end

  def ping_session(conn, _params) do
    Logger.info("Ping session, user: #{get_session(conn, :user_id)}")
    render conn, "pong.html"
  end

  def ping_session_deep(conn, _params) do
    Logger.info("Ping session deep, user: #{get_session(conn, :user).id}")
    render conn, "pong.html"
  end

  def login(conn, _params) do
    conn = fetch_query_params(conn)
    user_id = conn.query_params["id"]
    conn
    |> put_session(:user_id, String.to_integer(user_id))
    |> render("login.html")
  end

  def login_deep(conn, _params) do
    conn = fetch_query_params(conn)
    user_id = conn.query_params["id"]
    conn
    |> put_session(:user, %{id: String.to_integer(user_id)})
    |> render("login.html")
  end

  def logout(conn, _params) do
    conn
    |> clear_session()
    |> render("logout.html")
  end
end
