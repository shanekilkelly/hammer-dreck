defmodule DreckWeb.Router do
  use DreckWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DreckWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/ping", PageController, :ping
    get "/ping_session", PageController, :ping_session
    get "/ping_session_deep", PageController, :ping_session_deep
    get "/login", PageController, :login
    get "/login_deep", PageController, :login_deep
    get "/logout", PageController, :logout
  end

  # Other scopes may use custom stacks.
  # scope "/api", DreckWeb do
  #   pipe_through :api
  # end
end
